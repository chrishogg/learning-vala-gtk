public class : Test : Gtk.Application {

    public Test() {
        Object (
            application_id: "com.github.thurdai.test",
            flags: Glib.ApplicationFlags.FLAGS_NONE
        );
    }

    protected override void activate() {

        Gtk.Window window = new Gtk.ApplicationWindow(this);
    
        window.title = "First Vala App";
        window.window_position = Gtk.WindowPosition.CENTER;
        window.set_default_size (400, 400);
                
        window.show_all();
    }

    public static int main(string[] args) {
        var test = new Test();
        return test.run (args);

    }
}

//  int main (string[] args){
//      Gtk.init (ref args);
//      Gtk.Window window = new Gtk.Window();
    
//      window.title = "First Vala App";
//      window.border_width = 10;
//      window.window_position = Gtk.WindowPosition.CENTER;
//      window.set_default_size (400, 400);

//      window.destroy.connect (Gtk.main_quit);

//      window.show_all();

//      Gtk.main ();
//      return 0;
//  }