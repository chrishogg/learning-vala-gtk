//  Create a public class for the "MyApp" method and initialize it as a Gtk.Application.
public class MyApp : Gtk.Application {

    //  Create the "MyApp" method.
    public MyApp () {
        //  Create an object to store the application properties.
        Object (
            //  inside the object, set the unique application id.
            application_id: "com.gitlab.chrishogg.learning-vala-gtk", 
            //  set flags to define the behaviour of the application (FLAGS_NONE = default behaviour).
            flags: ApplicationFlags.FLAGS_NONE
        );
    }
    //  Override the GLib.Application "activate" signal*.
    protected override void activate () {

        //  Create a new application window called "main_window".
        var main_window = new Gtk.ApplicationWindow (this);
        //  Set the default height of the window.
        main_window.default_height = 300;
        //  Set the default width of the window.
        main_window.default_width = 300;
        //  Set the title of the window.
        main_window.title = (_("Hello World"));

        //  Create a label called "label".
        var label = new Gtk.Label (_("Hello World Again!"));
        
        //  Add "label" to the "main_window"
        main_window.add (label);

        //  Show everything within "main_window".
        main_window.show_all();
    }
    //  Create a public method called "main" that returns an integer.
    //  Pass in an empty string array and args* as peramiters.
    public static int main (string[] args) {
        //  create a variable called "app" and assign the "MyApp" Method to it.
        var app = new MyApp ();
        //  Run "app" and pass in args* as a peramiter.
        return app.run (args);
    }
}